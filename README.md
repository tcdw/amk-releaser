# AMK Releaser

## Before Use

Copy `config.example.json` to `config.json`, and editing properties in `config.json`:

- `siteCookie`: SMW Central site cookie, which you can cind with your browser's developer tool. The value should be something like this:

```text
tz_offset=-480; smwc_session=AAAAAAAAAAAAAAAAAAAAA; ageg=1
```

- `proxy`: If your need proxy for accessing outer Internet, you need this. Otherwise, leave it blank.

## File Naming Convention

The source file name, SPC file name and sample folder name (if used) of each music should be the same.

Example:

```text
Underwater.txt
Underwater.spc
Underwater/1.brr
Underwater/2.brr
Underwater/3.brr
```

## Package Port(s)

```bash
npm run package
```

## Submit Port

```bash
npm run submit
```
