export interface MusicSubmission {
    // token?: string
    // file?: Buffer
    fileName: string
    name: string
    namePrefix: string
    nameSuffix: string
    size: string
    type: string
    samples: string
    source: string
    duration: string
    featured: boolean
    description: string
    authors: string // 用半角逗号分割成数组，逐个进行 trim，将数组转换成若干 authors[]
    tags: string // 同上
    notes: string
    // skip_moderation: boolean
    skipModeration: boolean // true 时为 1，false 时直接不包含
}
