import { invert } from "lodash-es"

export const MUSIC_TYPE_MAP: Record<string, string> = {
    "song": "Song",
    "soundtrack": "Soundtrack",
    "sound_effect": "Sound Effect",
    "misc": "Misc."
}

export const MUSIC_TYPE_MAP_R = invert(MUSIC_TYPE_MAP);

export const SAMPLE_USAGE_MAP: Record<string, string> = {
    "none": "None",
    "light": "Light",
    "many": "Many"
}

export const SAMPLE_USAGE_MAP_R = invert(SAMPLE_USAGE_MAP);

export const SOURCE_MAP: Record<string, string> = {
    "port": "Port",
    "remix": "Remix",
    "original": "Original"
}

export const SOURCE_MAP_R = invert(SOURCE_MAP);
