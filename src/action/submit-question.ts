import { MusicSubmission } from "../types/submission";
import inquirer from 'inquirer';
import fs from "fs-extra";
import path from "path";
import readAMKStats from "../utils/read-amk-stats";

export default async function submitQuestions(template: MusicSubmission) {
    const { fileName } = await inquirer.prompt({
            type: 'rawlist',
            name: 'fileName',
            message: "Which one you would like to submit?",
            choices: fs.readdirSync("./out").map((e) => path.parse(e).name),
    });

    // read amk stats
    const amkStatsRaw = fs.readFileSync("../stats/" + fileName + ".txt", { encoding: "utf8" });
    const amkStats = readAMKStats(amkStatsRaw);
    const size = amkStats["SONG TOTAL DATA SIZE"];
    const durationNum = Number(amkStats["SONG TOTAL LENGTH IN SECONDS"]);
    const duration = Math.floor(durationNum / 60) + ":" + String(Math.floor(durationNum % 60)).padStart(2, "0");

    const result: MusicSubmission = await inquirer.prompt([
        {
            type: 'input',
            name: 'name',
            message: "Name",
            default: fileName || template.name || undefined,
        },
        {
            type: 'input',
            name: 'namePrefix',
            message: "Name Prefix",
            default: template.namePrefix || undefined,
        },
        {
            type: 'input',
            name: 'nameSuffix',
            message: "Name Suffix",
            default: template.nameSuffix || undefined,
        },
        {
            type: 'input',
            name: 'size',
            message: "Insert Size",
            default: size || template.size || undefined,
        },
        {
            type: 'list',
            name: 'type',
            message: "Type",
            default: template.type || undefined,
            choices: [
                "Song",
                "Soundtrack",
                "Sound Effect",
                "Misc."
            ]
        },
        {
            type: 'list',
            name: 'samples',
            message: "Sample Usage",
            default: template.samples || undefined,
            choices: [
                "None",
                "Light",
                "Many"
            ]
        },
        {
            type: 'list',
            name: 'source',
            message: "Source",
            default: template.source || undefined,
            choices: [
                "Port",
                "Remix",
                "Original"
            ]
        },
        {
            type: 'input',
            name: 'duration',
            message: "Duration",
            default: duration || template.duration || undefined,
        },
        {
            type: 'confirm',
            name: 'featured',
            message: "Featured",
            default: template.featured,
        },
        {
            type: 'input',
            name: 'description',
            message: "Description",
            default: template.description || undefined,
        },
        {
            type: 'input',
            name: 'authors',
            message: "Authors",
            default: template.authors || undefined,
        },
        {
            type: 'input',
            name: 'tags',
            message: "Tags",
            default: template.tags || undefined,
        },
        {
            type: 'input',
            name: 'notes',
            message: "Submission Notes",
            default: template.notes || undefined,
        },
        {
            type: 'confirm',
            name: 'skipModeration',
            message: "Skip Moderation",
            default: template.skipModeration,
        },
    ], { fileName });
    if (result.size.trim().startsWith("0x")) {
        result.size = result.size.trim().slice(2);
    }
    console.log(result);
    const { isRight } = await inquirer.prompt({
        type: 'confirm',
        name: 'isRight',
        message: "Is it right?",
    })
    if (!isRight) {
        return submitQuestions(result);
    }
    return result;
}
