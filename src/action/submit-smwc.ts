import axios from "axios";
import FormData from "form-data";
import { HttpsProxyAgent } from "https-proxy-agent";
import fs from "fs-extra";
import { MusicSubmission } from "../types/submission";
import { MUSIC_TYPE_MAP_R, SAMPLE_USAGE_MAP_R, SOURCE_MAP_R } from "../constants/trans-dict";
import config from "../../config.json";
import inquirer from 'inquirer';

export default async function submitToSMWC(data: MusicSubmission) {
    console.log("Gathering token ...");

    // create axios instance with proxy settings
    let request = axios.create({})
    if (config.proxy) {
        const httpsAgent = new HttpsProxyAgent(new URL(config.proxy))
        request = axios.create({
            httpsAgent
        })
    }

    // get token
    const result = await request({
        url: "https://www.smwcentral.net/?p=section&a=submit&s=smwmusic",
        method: "get",
        headers: {
            "cookie": config.siteCookie,
        },
    })
    const token = (result.data as string)
        .split('<input type="hidden" name="token" value="')[1]
        .split('">')[0];

    console.log("Token: " + token);

    // submit!!
    console.log("Submitting ...");

    // apply formData
    let formData = new FormData();
    formData.append('token', token);
    formData.append('MAX_FILE_SIZE', "10485760");
    formData.append('file', fs.readFileSync("./out/" + data.fileName + ".zip"), {
        filename: data.fileName + ".zip",
    });
    formData.append('name', data.namePrefix + data.name + data.nameSuffix);
    formData.append('size', data.size);
    formData.append('type', MUSIC_TYPE_MAP_R[data.type]);
    formData.append('samples', SAMPLE_USAGE_MAP_R[data.samples]);
    formData.append('source', SOURCE_MAP_R[data.source]);
    formData.append('duration', data.duration);
    if (data.featured) {
        formData.append('featured', "1");
    }
    formData.append('description', data.description);
    data.authors.split(",")
        .map(e => e.trim())
        .forEach((e) => {
            formData.append('authors[]', e);
        })
    data.tags.split(",")
        .map(e => e.trim())
        .forEach((e) => {
            formData.append('tags[]', e);
        })
    formData.append('notes', data.notes);
    if (data.skipModeration) {
        formData.append('skip_moderation', "1");
    }
    formData.append('submit', "Submit");

    // fire
    const resultSubmit = await request({
        url: "https://www.smwcentral.net/?p=section&a=submit&s=smwmusic",
        method: "post",
        data: formData,
        headers: {
            "cookie": config.siteCookie,
            "referer": "https://www.smwcentral.net/?p=section&a=submit&s=smwmusic"
        },
    })
    const resultHTML: string = resultSubmit.data;
    if (resultHTML.includes("File submitted successfully.")) {
        console.log("Success!");
    } else {
        console.log("Failed!");
        console.log(resultHTML);
        return;
    }

    const { isDelete } = await inquirer.prompt({
        type: 'confirm',
        name: 'isDelete',
        message: "Delete local packaged submission?",
    })
    if (isDelete) {
        fs.rmSync("./out/" + data.fileName + ".zip");
    }
}