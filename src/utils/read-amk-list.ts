export default function readAMKList(raw: string) {
    const line = raw.split("\n")
        .map((e) => e.trim())
        .filter((e) => e);

    const obj: Record<string, Record<number, string>> = {};
    let currentKey = "";
    line.forEach((e) => {
        if (e.endsWith(":")) {
            currentKey = e.slice(0, -1);
            return;
        }
        const found = /^((?:[0-9]|[A-F]){2})  (.+)$/.exec(e);
        if (found) {
            if (!obj[currentKey]) {
                obj[currentKey] = {};
            }
            obj[currentKey][parseInt(found[1], 16)] = found[2];
        }
    })

    return obj;
}
