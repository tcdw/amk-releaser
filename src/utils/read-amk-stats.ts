export default function readAMKStats(raw: string) {
    const line = raw.split("\n")
        .map((e) => e.trim())
        .filter((e) => e);

    const obj: Record<string, string> = {};
    let currentKey = "";
    line.forEach((e) => {
        const found = /^(.+):\s+(.+)$/.exec(e);
        if (found) {
            obj[found[1]] = found[2];
        }
    })

    return obj;
}

/*
const example = `
CHANNEL 0 SIZE:				0x002A
CHANNEL 1 SIZE:				0x01AE
CHANNEL 2 SIZE:				0x0010
CHANNEL 3 SIZE:				0x001E
CHANNEL 4 SIZE:				0x0017
CHANNEL 5 SIZE:				0x0023
CHANNEL 6 SIZE:				0x0000
CHANNEL 7 SIZE:				0x0000
LOOP DATA SIZE:				0x0183
POINTERS AND INSTRUMENTS SIZE:		0x0022
SAMPLES SIZE:				0x6725
ECHO SIZE:				0x4800
SONG TOTAL DATA SIZE:			0x03E5
FREE ARAM (APPROXIMATE):		0x48A8

CHANNEL 0 TICKS:			0x6144
CHANNEL 1 TICKS:			0x6144
CHANNEL 2 TICKS:			0x6144
CHANNEL 3 TICKS:			0x6144
CHANNEL 4 TICKS:			0x6240
CHANNEL 5 TICKS:			0x6144
CHANNEL 6 TICKS:			0x0000
CHANNEL 7 TICKS:			0x0000

SONG INTRO LENGTH IN SECONDS:		0
SONG MAIN LOOP LENGTH IN SECONDS:	66.7826
SONG TOTAL LENGTH IN SECONDS:		66.7826
`

console.log(readAMKStats(example));
*/
