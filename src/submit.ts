import inquirer from 'inquirer';
import fs from "fs-extra";
import { MusicSubmission } from './types/submission';
import submitToSMWC from './action/submit-smwc';
import submitQuestions from './action/submit-question';

const templatePath = "./templates";

try {
    // select template
    const { templateSelection } = await inquirer.prompt({
        type: 'list',
        name: 'templateSelection',
        message: "What submission template do you want to use?",
        choices: fs.readdirSync(templatePath).map((e) => e.slice(0, -5)),
    })

    // user input
    const template: MusicSubmission = fs.readJSONSync(templatePath + "/" + templateSelection + ".json", { encoding: "utf8" });
    const result = await submitQuestions(template);

    await submitToSMWC(result);
} catch (e) {
    if ((e as any)?.isTtyError) {
        console.error("Sorry, interactive shell is required!!");
    } else {
        console.error(e);
    }
    process.exit(1);
}
