import JSZip from "jszip";
import fs from "fs-extra";
import inquirer from 'inquirer';
import readAMKList from "./utils/read-amk-list";
import { last } from "lodash-es";

try {
    const inputs = await inquirer.prompt([
        {
            type: 'input',
            name: 'from',
            message: "Track ID From (Hex)",
            validate(input) {
                if (isNaN(parseInt(input, 16))) {
                    return "Please enter a hex number";
                }
                return true;
            },
        },
        {
            type: 'input',
            name: 'to',
            message: "Track ID To (Hex)",
            validate(input) {
                if (isNaN(parseInt(input, 16))) {
                    return "Please enter a hex number";
                }
                return true;
            },
        },
    ]);

    const amkListRaw = fs.readFileSync("../Addmusic_list.txt", { encoding: "utf8" });
    const amkList = readAMKList(amkListRaw).Locals;
    const from = parseInt(inputs.from, 16);
    const to = parseInt(inputs.to, 16);
    let i = from;

    // fs.rmSync("./out", { recursive: true, force: true });
    fs.mkdirpSync("./out");

    while (i <= to) {
        const e = amkList[i];
        const logTitle = "[" + e + "] ";
        console.log(logTitle + "Processing ...");
        const zip = new JSZip();
        const realName = e.slice(0, -4);

        // read txt and spc
        zip.file(realName + ".txt", fs.readFileSync("../music/" + realName + ".txt"));
        zip.file(realName + ".spc", fs.readFileSync("../SPCs/" + last(realName.split("/")) + ".spc"));

        // if sample dir exists
        const samplePathTry = fs.readFileSync("../music/" + realName + ".txt", { encoding: "utf8" }).split('#path "')[1];
        if (samplePathTry) {
            const actualName = samplePathTry.split('"')[0];
            const list = fs.readdirSync("../samples/" + actualName);
            const brr = zip.folder(actualName)!;
            list.forEach((f) => {
                console.log(logTitle + "Adding sample " + f + " ...");
                brr.file(f, fs.readFileSync("../samples/" + actualName + "/" + f));
            })
        }

        // save zip!!
        const buf = await zip.generateAsync({
            type: "uint8array",
            compression: "DEFLATE",
            compressionOptions: {
                level: 9
            }
        });
        fs.mkdirpSync("./out");
        fs.writeFileSync("./out/" + last(realName.split("/")) + ".zip", buf);
        i++;
    }
} catch (e) {
    if ((e as any)?.isTtyError) {
        console.error("Sorry, interactive shell is required!!");
    } else {
        console.error(e);
    }
    process.exit(1);
}
